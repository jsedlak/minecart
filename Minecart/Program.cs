﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Minecart.Core.DataModel;
using Minecart.Core.ServiceModel;
using Minecart.Core.Services;

namespace Minecart
{
    class Program
    {
        private const string LineBreak = "##################################################";

        static void Main(string[] args)
        {
            var zecAddress = "t1efD9pXaYsEUpwgDbUbganTfbMm62RgohG";
            var siaAddress = "2025b077f1c5c2a70f6da0dc8415c8ad4ae91f3dc97da2bf9ae677ff397c184a7150a10cbf3a";
            var ethAddress = "0x545536f2b663893743bdc581fb7907dc846c3803";

            var p = new Program();

            while (true)
            {
                p.Update<ZCoin>(zecAddress);
                p.Update<SiaCoin>(siaAddress);
                p.Update<EthCoin>(ethAddress);

                Thread.Sleep(TimeSpan.FromSeconds(15));
                Console.Clear();
            }
        }

        private readonly ICoinServiceFactory _factory = new MemoryCoinServiceFactory();

        public Program()
        {
            _factory.Register(new ZecNanoService());
            _factory.Register(new EthNanoService());
            _factory.Register(new SiaNanoService());
        }

        public async Task Update<TCoin>(string address) where TCoin : ICoin
        {
            var repo = _factory.Get<TCoin>();

            var balance = (await repo.GetBalanceAsync(address)).Amount;
            var hashrate = await repo.GetCurrentHashrateAsync(address);
            var avg = (await repo.GetAvgHashrateAsync(address)).SixHour;

            Console.WriteLine(LineBreak);
            Console.WriteLine($"{typeof(TCoin).Name} Account: {address}");
            Console.WriteLine($"\tBalance: {balance}");
            Console.WriteLine($"\tHashrate: {hashrate}");
            Console.WriteLine($"\t6hr Avg: {avg}");
        }
    }   
}