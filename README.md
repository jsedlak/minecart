# Minecart

![Minecart Logo](https://gitlab.com/jsedlak/minecart/raw/master/Minecart.Win/Assets/SplashScreen.scale-200.png)

A little pet project for monitoring my miners without having to RDP to machines or refresh browsers. Updates via API on regular (configurable) basis (default: 60 seconds).

Currently works with **Nanopool** for `Ethereum`, `ZCash`, and `SiaCoin`. Stores all data in your `AppData` folder. Can be extended via .NET libraries.

## Service Extensions

To build your own Pool Service (responsible for responding to data requests/updates), implement the `IPoolService<TCoin>` interface from `Minecart.Core.dll`. Drop the built library in the same location as the executable, and change the config (`AppData\~\minecart.config.json`) to add your service.

## Donations
I've spent some considerable time and blah blah blah doing this for free.

If you think my code is worthy of any praise, I ask that you donate coin to at least one of the following addresses.

### Ethereum
    0xDdD146F3873CE77b7a630cF99dD0c41C37aB9f51

### Sia Coin
    ef3c21804e1254b1010ce96fb68efe3988023e31d19919d08129467d80dca11c60f59fc061ea

### Bitcoin
    1CuXkk7eSk7LPetVUVqMFEdgamGyxbiCZy


