﻿using Minecart.DataModel;

namespace Minecart.Services
{
    public class EthNanoService : NanoPoolService<EthCoin>
    {
        public EthNanoService()
            : base("https://api.nanopool.org/v1/eth/")
        {
        }
    }
}
