﻿using Minecart.DataModel;

namespace Minecart.Services
{
    public class ZecNanoService : NanoPoolService<ZCoin>
    {
        public ZecNanoService()
            : base("https://api.nanopool.org/v1/zec/")
        {
        }
    }
}
