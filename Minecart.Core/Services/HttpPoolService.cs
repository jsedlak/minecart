﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Minecart.DataModel;
using Minecart.ServiceModel;

namespace Minecart.Services
{
    public abstract class HttpPoolService<TCoin> : IPoolService<TCoin> where TCoin : ICoin
    {
        private readonly HttpClient _client;
        private readonly string _baseUrl;

        protected HttpPoolService(string baseUrl)
        {
            _baseUrl = baseUrl;
            _client = new HttpClient();
        }

        protected virtual async Task<string> Get(string path)
        {
            try
            {
                return await _client.GetStringAsync(_baseUrl + path);
            }
            catch
            {
                return "{}";
            }
        }

        public abstract Task<EarningsEstimate> GetEarningsEstimateAsync(double hashrate);

        public abstract Task<AccountStatus> GetAccountStatusAsync(string address);

        public abstract Task<AverageHashrateData> GetAvgHashrateAsync(string address);

        public abstract Task<double> GetCurrentHashrateAsync(string address);

        public abstract Task<ICoin> GetBalanceAsync(string address);

        public abstract Task<IEnumerable<Worker>> GetWorkersAsync(string address);
    }
}
