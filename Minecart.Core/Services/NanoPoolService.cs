﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Minecart.ApiModel;
using Minecart.DataModel;
using Newtonsoft.Json;

namespace Minecart.Services
{
    public class NanoPoolService<TCoin> : HttpPoolService<TCoin> where TCoin : ICoin, new()
    {
        public NanoPoolService(string baseUrl)
            : base(baseUrl)
        {
        }

        public override async Task<EarningsEstimate> GetEarningsEstimateAsync(double hashrate)
        {
            var result = await Get($"approximated_earnings/{hashrate}");
            return JsonConvert.DeserializeObject<NanoPoolApiPayload<EarningsEstimate>>(result).Data;
        }

        public override async Task<AccountStatus> GetAccountStatusAsync(string address)
        {
            var result = await Get($"user/{address}");
            return JsonConvert.DeserializeObject<NanoPoolApiPayload<AccountStatus>>(result).Data;
        }

        public override async Task<AverageHashrateData> GetAvgHashrateAsync(string address)
        {
            var result = await Get($"avghashrate/{address}");
            return JsonConvert.DeserializeObject<NanoPoolApiPayload<AverageHashrateData>>(result).Data;
        }

        public override async Task<double> GetCurrentHashrateAsync(string address)
        {
            var result = await Get($"balance_hashrate/{address}");
            return JsonConvert.DeserializeObject<NanoPoolApiPayload<BalanceHashrateData>>(result).Data.Hashrate;
        }

        public override async Task<ICoin> GetBalanceAsync(string address)
        {
            var result = await Get($"balance_hashrate/{address}");

            return new TCoin()
            {
                Amount = JsonConvert.DeserializeObject<NanoPoolApiPayload<BalanceHashrateData>>(result).Data.Balance
            };
        }

        public override async Task<IEnumerable<Worker>> GetWorkersAsync(string address)
        {
            var result = await Get($"workers/{address}");
            return JsonConvert.DeserializeObject<NanoPoolApiPayload<IEnumerable<Worker>>>(result).Data;
        }
    }
}
