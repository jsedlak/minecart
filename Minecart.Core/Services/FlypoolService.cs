﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Minecart.DataModel;
using Minecart.ServiceModel;
using Newtonsoft.Json;

namespace Minecart.Services
{
    public abstract class FlypoolService<TCoin> : HttpPoolService<TCoin> where TCoin : ICoin, new()
    {
        protected FlypoolService()
            : base("http://zcash.flypool.org/api/")
        {
        }

        public override async Task<AccountStatus> GetAccountStatusAsync(string address)
        {
            try
            {
                var result = await Get($"miner_new/{address}");
                var status = JsonConvert.DeserializeObject<FlypoolAccountStatusResult>(result);

                return new AccountStatus
                {
                    AvgHashrate = new AverageHashrateData
                    {
                        OneHour = status.GetHashrate(),
                        ThreeHour = status.AvgHashrate,
                        SixHour = status.AvgHashrate,
                        TwentyFourHour = status.AvgHashrate
                    },
                    Balance = status.Unpaid,
                    CurrentHashrate = status.GetReportedHashrate(),
                    UnconfirmedBalance = 0,
                    Workers = status.Workers
                };
            }
            catch (JsonSerializationException jse)
            {
                Debug.WriteLine(jse.Message);
            }

            return null;
        }

        public override async Task<EarningsEstimate> GetEarningsEstimateAsync(double hashrate)
        {
            return new EarningsEstimate();
        }

        public override async Task<AverageHashrateData> GetAvgHashrateAsync(string address)
        {
            var status = await GetAccountStatusAsync(address);
            return status.AvgHashrate;
        }

        public override async Task<ICoin> GetBalanceAsync(string address)
        {
            var status = await GetAccountStatusAsync(address);
            return new TCoin {Amount = status.Balance};
        }

        public override async Task<double> GetCurrentHashrateAsync(string address)
        {
            var status = await GetAccountStatusAsync(address);
            return status.CurrentHashrate;
        }

        public override async Task<IEnumerable<Worker>> GetWorkersAsync(string address)
        {
            var status = await GetAccountStatusAsync(address);
            return status.Workers;
        }
    }

    [JsonObjectAttribute]
    public class FlypoolAccountStatusResult
    {
        private double Convert(string hashrate)
        {
            var val = "";
            foreach (char t in hashrate)
            {
                var code = (byte)t;
                if (code == 46 || (code >= 48 && code <= 57))
                {
                    val += t;
                }
            }
            return double.Parse(val);
        }

        public double GetHashrate() => Convert(Hashrate);
        public double GetReportedHashrate() => Convert(ReportedHashrate);

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("hashRate")]
        public string Hashrate { get; set; }

        [JsonProperty("reportedHashRate")]
        public string ReportedHashrate { get; set; }

        [JsonProperty("unpaid")]
        public double Unpaid { get; set; }

        [JsonProperty("usdPerMin")]
        public double UsdPerMin { get; set; }

        [JsonProperty("avgHashrate")]
        public double AvgHashrate { get; set; }

        [JsonProperty("workers_array")]
        public IEnumerable<Worker> Workers { get; set; }
    }
}
