﻿using System.Collections.Generic;
using Minecart.DataModel;
using Minecart.ServiceModel;

namespace Minecart.Core.Services
{
    public class MemoryCoinServiceFactory : ICoinServiceFactory
    {
        private readonly Dictionary<string, IPoolService> _repo = new Dictionary<string, IPoolService>();
        
        
        public IPoolService Get(string name)
        {
            return _repo[name];
        }

        public void Register(IPoolService service)
        {
            _repo.Add(service.GetType().AssemblyQualifiedName, service);
        }

        public void Register(string name, IPoolService service)
        {
            _repo.Add(name, service);
        }
    }
}
