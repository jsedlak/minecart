﻿using Minecart.DataModel;

namespace Minecart.Services
{
    public class SiaNanoService : NanoPoolService<SiaCoin>
    {
        public SiaNanoService()
            : base("https://api.nanopool.org/v1/sia/")
        {
        }
    }
}
