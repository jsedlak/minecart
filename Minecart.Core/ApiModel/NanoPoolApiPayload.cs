﻿namespace Minecart.ApiModel
{
    public class NanoPoolApiPayload<TDataModel>
    {
        public bool Status { get; set; }
        public TDataModel Data { get; set; }
    }
}
