﻿using Minecart.DataModel;

namespace Minecart.ServiceModel
{
    public interface ICoinServiceFactory
    {
        IPoolService Get(string name);
        void Register(IPoolService service);
        void Register(string name, IPoolService service);
    }
}
