﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Minecart.DataModel;

namespace Minecart.ServiceModel
{
    public interface IPoolService
    {
        Task<AccountStatus> GetAccountStatusAsync(string address);

        Task<AverageHashrateData> GetAvgHashrateAsync(string address);
        Task<double> GetCurrentHashrateAsync(string address);

        Task<ICoin> GetBalanceAsync(string address);

        Task<IEnumerable<Worker>> GetWorkersAsync(string address);

        Task<EarningsEstimate> GetEarningsEstimateAsync(double hashrate);
    }

    public interface IPoolService<TCoin> : IPoolService where TCoin : ICoin
    {
    }
}
