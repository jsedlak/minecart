﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Minecart.DataModel
{
    public class AccountStatus
    {
        [JsonProperty("balance")]
        public double Balance { get; set; }

        [JsonProperty("unconfirmed_balance")]
        public double UnconfirmedBalance { get; set; }

        [JsonProperty("hashrate")]
        public double CurrentHashrate { get; set; }

        [JsonProperty("avghashrate")]
        public AverageHashrateData AvgHashrate { get; set; }

        [JsonProperty("worker")]
        public IEnumerable<Worker> Workers { get; set; }
    }
}
