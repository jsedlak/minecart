﻿namespace Minecart.DataModel
{
    public class Worker
    {
        public string Id { get; set; }
        public double Hashrate { get; set; }
        public int Rating { get; set; }
    }
}
