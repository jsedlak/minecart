﻿namespace Minecart.DataModel
{
    public class BalanceHashrateData
    {
        public double Hashrate { get; set; }

        public double Balance { get; set; }
    }
}
