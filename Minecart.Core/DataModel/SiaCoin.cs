﻿namespace Minecart.DataModel
{
    public class SiaCoin : ICoin
    {
        public static int Multiplier = 1;

        public double NormalizedAmount => Amount / Multiplier;

        public double Amount { get; set; }
    }
}
