using System.ComponentModel;
using System.Runtime.CompilerServices;
using Minecart.Annotations;
using Newtonsoft.Json;

namespace Minecart.DataModel
{
    public class EarningsEstimateValue : INotifyPropertyChanged
    {
        private double _coins;
        private double _bitcoins;
        private double _dollars;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        [JsonProperty("coins")]
        public double Coins
        {
            get => _coins;
            set
            {
                _coins = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("bitcoins")]
        public double Bitcoins
        {
            get => _bitcoins;
            set
            {
                _bitcoins = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("dollars")]
        public double Dollars
        {
            get => _dollars;
            set
            {
                _dollars = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        
    }
}