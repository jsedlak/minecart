﻿using Newtonsoft.Json;

namespace Minecart.DataModel
{
    public class AverageHashrateData
    {
        [JsonProperty("h1")]
        public double OneHour { get; set; }

        [JsonProperty("h3")]
        public double ThreeHour { get; set; }

        [JsonProperty("h6")]
        public double SixHour { get; set; }

        [JsonProperty("h12")]
        public double TwelveHour { get; set; }

        [JsonProperty("h24")]
        public double TwentyFourHour { get; set; }
    }
}
