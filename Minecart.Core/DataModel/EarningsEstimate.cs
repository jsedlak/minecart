﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Minecart.Annotations;
using Newtonsoft.Json;

namespace Minecart.DataModel
{
    public class EarningsEstimate : INotifyPropertyChanged
    {
        private EarningsEstimateValue _minute = new EarningsEstimateValue();
        private EarningsEstimateValue _hour = new EarningsEstimateValue();
        private EarningsEstimateValue _day = new EarningsEstimateValue();
        private EarningsEstimateValue _week = new EarningsEstimateValue();
        private EarningsEstimateValue _month = new EarningsEstimateValue();

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        [JsonProperty("minute")]
        public EarningsEstimateValue Minute
        {
            get => _minute;
            set
            {
                _minute = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("hour")]
        public EarningsEstimateValue Hour
        {
            get => _hour;
            set
            {
                _hour = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("day")]
        public EarningsEstimateValue Day
        {
            get => _day;
            set
            {
                _day = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("week")]
        public EarningsEstimateValue Week
        {
            get => _week;
            set
            {
                _week = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("month")]
        public EarningsEstimateValue Month
        {
            get => _month;
            set
            {
                _month = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}