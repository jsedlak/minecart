﻿namespace Minecart.DataModel
{
    public class EthCoin : ICoin
    {
        public static int Multiplier = 1;

        public double NormalizedAmount => Amount / Multiplier;

        public double Amount { get; set; }
    }
}
