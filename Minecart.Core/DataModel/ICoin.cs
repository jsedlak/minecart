﻿namespace Minecart.DataModel
{
    public interface ICoin
    {
        double NormalizedAmount { get; }

        double Amount { get; set; }
    }
}
