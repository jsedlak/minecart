﻿namespace Minecart.DataModel
{
    public class ZCoin : ICoin
    {
        public static int Multiplier = 1;

        public double NormalizedAmount => Amount / Multiplier;

        public double Amount { get; set; }
    }
}
