﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Minecart.Core.Services;
using Minecart.DataModel;
using Minecart.ServiceModel;
using Minecart.Services;
using Minecart.Win.DataModel;
using Minecart.Win.DataModel.Config;
using Minecart.Win.ServiceModel;
using Minecart.Win.Services;
using Newtonsoft.Json;
using Syncfusion.UI.Xaml.Charts;
using Syncfusion.UI.Xaml.Controls.Layout;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Minecart.Win
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public const string SettingsFilename = "minecart.config.json";
        public static MinecartSettings Settings { get; set; }

        private readonly ICoinServiceFactory _factory = new MemoryCoinServiceFactory();
        private readonly IConfigService _configService = new AppDataConfigService();

        private readonly ObservableCollection<MiningAccount> _accounts = new ObservableCollection<MiningAccount>();

        private Timer _timer;

        public MainPage()
        {
            ApplicationView.GetForCurrentView().Title = "Minecart v" + GetType().GetTypeInfo().Assembly.GetName().Version;

            this.InitializeComponent();

            _accounts.CollectionChanged += AccountsCollectionChanged;

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                InitializeApp();
            });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }

        private async Task InitializeApp()
        {
            Settings = await _configService.Load(
                SettingsFilename,
                () => new MinecartSettings
                {
                    Interval = 60,
                    SeriesType = typeof(SplineSeries).AssemblyQualifiedName,
                    Version = this.GetType().GetTypeInfo().Assembly.GetName().Version.ToString(),
                    Services = new[]
                    {
                        new PoolServiceConfig { Name = "Zec Nano", Coin = "ZCash", ServiceType = typeof(ZecNanoService).AssemblyQualifiedName },
                        new PoolServiceConfig { Name = "Eth Nano", Coin = "Eth", ServiceType = typeof(EthNanoService).AssemblyQualifiedName },
                        new PoolServiceConfig { Name = "Sia Nano", Coin = "Sia", ServiceType = typeof(SiaNanoService).AssemblyQualifiedName },
                        new PoolServiceConfig { Name = "Zec Flypool", Coin = "ZCash", ServiceType = typeof(ZCashFlypoolService).AssemblyQualifiedName }, 
                    }
                }
            );

            foreach (var serviceConfig in Settings.Services)
            {
                _factory.Register(
                    serviceConfig.Name,
                    (IPoolService)Activator.CreateInstance(Type.GetType(serviceConfig.ServiceType))
                );
            }

            foreach (var accConfig in Settings.Accounts)
            {
                var account =
                    await _configService.Load(
                        accConfig.Filename,
                        () => new MiningAccount(accConfig.Name, null)
                    );

                _accounts.Add(account);
            }

            AddDashboardHub();

            // Create the timer based on our interval in the settings, starting from the next minute
            var target = DateTime.Now.AddMinutes(1);
            target = new DateTime(target.Year, target.Month, target.Day, target.Hour, target.Minute, 0, 0);
            _timer = new Timer(TimerOnTick, new object(), target - DateTime.Now, TimeSpan.FromSeconds(Settings.Interval));
        }

        #region Timer Stuff
        private async void TimerOnTick(object state)
        {
            var now = DateTime.Now;
            Debug.WriteLine($"Checking at {now}");

            foreach (var acc in _accounts)
            {
                try
                {
                    var poolService = _factory.Get(acc.PoolServiceName);
                    var accStatus = await poolService.GetAccountStatusAsync(acc.Address);

                    if (accStatus == null)
                    {
                        Debug.WriteLine($"Account status was null for account: {acc.Name}!");
                        continue;
                    }

                    var accEstimate = await poolService.GetEarningsEstimateAsync(accStatus.AvgHashrate.SixHour);

                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        acc.CurrentBalance = accStatus.Balance;
                        acc.AvgHashrate = accStatus.AvgHashrate.SixHour;
                        acc.CurrentHashrate = accStatus.CurrentHashrate;
                        acc.UnconfirmedBalance = accStatus.UnconfirmedBalance;
                        acc.CurrentEstimate = accEstimate ?? new EarningsEstimate();

                        acc.HashrateHistory.Add(new ValueAtTime(accStatus.CurrentHashrate, now));
                        acc.AvgHashrateHistory.Add(new ValueAtTime(accStatus.AvgHashrate.SixHour, now));
                        acc.BalanceHistory.Add(new ValueAtTime(accStatus.Balance, now));

                        Debug.WriteLine($"Balance for {acc.Name} is {accStatus.Balance} ({accStatus.Balance})");
                        StatusBlock.Text = $"Last Update was {now:HH:mm:ss}";
                    });
                }
                catch
                {
                    Debug.WriteLine($"Could not update account: {acc.Name}!");
                }
            }

            // Save all the accounts
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, SaveAllData);
        }
        #endregion

        #region Dock Management

        private void AddDockedWindow(string header, object control)
        {
            var contentControl = new ContentControl
            {
                Content = control
            };

            SfDockingManager.SetDockState(contentControl, DockState.Document);
            SfDockingManager.SetHeader(contentControl, header);

            DockManager.Children.Add(contentControl);
        }

        private void AddDashboardHub()
        {
            AddDockedWindow("DASHBOARD", new DashboardHub(_accounts));
        }

        private void AddHubWindow(MiningAccount account)
        {
            AddDockedWindow($"{account.Name} Hub", new AccountHub(account));
        }

        private void AddChartWindow(MiningAccount account)
        {
            AddDockedWindow($"{account.Name} Watch", new AccountWatch(account));
        }
        #endregion

        #region Event Handlers
        private void AccountsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var acc in e.NewItems)
                    {
                        var miningAccount = acc as MiningAccount;

                        if (miningAccount == null)
                        {
                            continue;
                        }

                        // AddChartWindow(miningAccount);
                        AddHubWindow(miningAccount);
                    }
                    break;
            }
        }

        private void ShowAccountsPanelMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            MainView.IsPaneOpen = !MainView.IsPaneOpen;
        }

        private void NewAccountMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                var dlg = new NewAccountDialog(Settings.Services);
                var result = await dlg.ShowAsync();

                if (result != ContentDialogResult.Primary)
                {
                    return;
                }

                // build the new account
                var accountConfig = new MiningAccountConfig
                {
                    Name = dlg.GetAccountName(),
                    Filename = dlg.GetAccountName() + ".account.json"
                };

                var miningAccount = new MiningAccount
                {
                    Name = dlg.GetAccountName(),
                    Address = dlg.GetAccountAddress(),
                    PoolServiceName = dlg.GetPoolServiceName()
                };

                // add it to the context
                Settings.Accounts.Add(accountConfig);
                _accounts.Add(miningAccount);

                // and save the data
                SaveAllData();
            });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }

        private void QuitAppMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            App.Current.Exit();
        }

        private void SaveAccounts_OnClick(object sender, RoutedEventArgs e)
        {
            var result = JsonConvert.SerializeObject(
                ExportAccounts(),
                Formatting.Indented,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                }
            );

            var control = new ContentControl
            {
                Content = new TextDocument(result)
            };

            SfDockingManager.SetDockState(control, DockState.Document);
            SfDockingManager.SetHeader(control, "Save Output");

            DockManager.Children.Add(control);
        }

        private void AboutMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            new AboutDialog().ShowAsync();
        }

        private void SourceMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            Windows.System.Launcher.LaunchUriAsync(new Uri("https://gitlab.com/jsedlak/minecart"));
        }

        private void ShowAccountsPanelMenuItem_OnChecked(object sender, RoutedEventArgs e)
        {
            MainView.IsPaneOpen = true;
            MainView.DisplayMode = SplitViewDisplayMode.Inline;
        }

        private void ShowAccountsPanelMenuItem_OnUnChecked(object sender, RoutedEventArgs e)
        {
            MainView.DisplayMode = SplitViewDisplayMode.Overlay;
        }

        private void ShowDashboard_OnClick(object sender, RoutedEventArgs e)
        {
            AddDashboardHub();
        }

        private void AppDataMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            // Windows.System.Launcher.LaunchUriAsync(new Uri());
            Launcher.LaunchFolderAsync(ApplicationData.Current.LocalFolder);
        }
        #endregion

        #region Data Management
        private IEnumerable<dynamic> ExportAccounts()
        {
            foreach (var acc in _accounts)
            {
                yield return ExportAccount(acc);
            }
        }

        private dynamic ExportAccount(MiningAccount account)
        {
            return new
            {
                name = account.Name,
                address = account.Address,
                currentBalance = account.CurrentBalance,
                unconfirmedBalance = account.UnconfirmedBalance,
                avgHashrate = account.AvgHashrate,
                currentHashrate = account.CurrentHashrate,
                currentEstimate = account.CurrentEstimate,
                poolServiceName = account.PoolServiceName,
                balanceHistory = account.BalanceHistory.ToArray(),
                hashrateHistory = account.HashrateHistory.ToArray(),
                avgHashrateHistory = account.AvgHashrateHistory.ToArray()
            };
        }

        private void SaveAllData()
        {
            _configService.Save(SettingsFilename, Settings);

            foreach (var acc in Settings.Accounts)
            {
                var miningAccount = _accounts.FirstOrDefault(m => m.Name.Equals(acc.Name, StringComparison.CurrentCultureIgnoreCase));
                _configService.Save(acc.Filename, ExportAccount(miningAccount));
            }
        }
        #endregion

        public ObservableCollection<MiningAccount> Accounts => _accounts;
    }
}
