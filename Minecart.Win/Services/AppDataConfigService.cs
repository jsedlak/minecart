﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Windows.Storage;
using Minecart.Win.ServiceModel;
using Newtonsoft.Json;

namespace Minecart.Win.Services
{
    public class AppDataConfigService : IConfigService
    {
        public async Task<TConfigModel> Load<TConfigModel>(string filename, Func<TConfigModel> createDefault)
        {
            var folder = ApplicationData.Current.LocalFolder;

            Debug.WriteLine($"Reading config from {folder.Path}, file={filename}");

            StorageFile file = null;

            try
            {
                file = await folder.GetFileAsync(filename);
            }
            catch (FileNotFoundException)
            {
                file = null;
            }

            if (file == null)
            {
                var defaultConfigObject = createDefault();
                var fileContents = JsonConvert.SerializeObject(defaultConfigObject, Formatting.Indented);

                file = await folder.CreateFileAsync(filename);
                await FileIO.WriteTextAsync(file, fileContents);

                return defaultConfigObject;
            }

            var data = await FileIO.ReadTextAsync(file);

            return JsonConvert.DeserializeObject<TConfigModel>(data);
        }

        public async Task Save<TConfigModel>(string filename, TConfigModel configModel)
        {
            var folder = ApplicationData.Current.LocalFolder;

            await FileIO.WriteTextAsync(
                await folder.CreateFileAsync(filename, CreationCollisionOption.OpenIfExists),
                JsonConvert.SerializeObject(configModel, Formatting.Indented)
            );
        }
    }
}
