﻿using System;
using System.Linq;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Minecart.Win.DataModel;
using Syncfusion.UI.Xaml.Charts;
using Windows.UI;
using Windows.UI.Core;
using Minecart.DataModel;
using Syncfusion.UI.Xaml.Spreadsheet.Converter;

namespace Minecart.Win
{
    public sealed partial class AccountWatch : UserControl
    {
        private static readonly Color PrimaryColor = Color.FromArgb(255, 26, 93, 127);
        private static readonly Color PrimaryColorHalfAlpha = Color.FromArgb(50, 26, 93, 127);
        private static readonly Color PrimaryDarkColor = Color.FromArgb(255, 15, 54, 74);
        private static readonly Color SecondaryColor = Color.FromArgb(255, 252, 104, 61);
        private static readonly Color SecondaryDarkColor = Color.FromArgb(255, 191, 79, 46);

        private MiningAccount _account;

        public AccountWatch()
        {
            InitializeComponent();

            Initialize();
        }

        public AccountWatch(MiningAccount account)
        {
            InitializeComponent();

            Initialize(account);
            SetWatch(account);
        }

        private void Initialize(MiningAccount account = null)
        {
            Chart.PrimaryAxis = new DateTimeAxis()
            {
                Interval = MainPage.Settings.Interval,
                IntervalType = DateTimeIntervalType.Seconds,
                Header = "Time",
                ShowTrackBallInfo = true,
                LabelFormat = "HH:mm:ss",
                Minimum = account?.BalanceHistory[0].Timestamp ?? DateTime.Now,
                Maximum = DateTime.Now.AddSeconds(MainPage.Settings.Interval * 2),
                EnableScrollBar = true
            };
        }

        public void SetWatch(MiningAccount account)
        {
            _account = account;

            _account.BalanceHistory.CollectionChanged += (sender, args) =>
            {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    Chart.PrimaryAxis.SetValue(
                        DateTimeAxis.MaximumProperty, 
                        _account.BalanceHistory.Last().Timestamp.AddSeconds(MainPage.Settings.Interval)
                    );
                });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            };

            // adds all the little boxes
            TileHub.AddHubControls(_account);

            Chart.Series.Clear();

            var balanceSeries = new FastLineSeries
            {
                ItemsSource = _account.BalanceHistory,
                Interior = PrimaryColorHalfAlpha.ConvertColorToBrush(),
                Stroke = PrimaryDarkColor.ConvertColorToBrush(),
                StrokeThickness = 2,
                Label = _account.Name + " Balance",
                XBindingPath = nameof(ValueAtTime.Timestamp),
                YBindingPath = nameof(ValueAtTime.Value),
                ShowTooltip = true,
                AdornmentsInfo = new ChartAdornmentInfo
                {
                    ShowMarker = true,
                    SymbolStroke = new SolidColorBrush(PrimaryColor),
                    SymbolInterior = new SolidColorBrush(PrimaryColor),
                    SymbolHeight = 10,
                    SymbolWidth = 10,
                    Symbol = ChartSymbol.Square
                },
                YAxis = new NumericalAxis
                {
                    ShowTrackBallInfo = true,
                    StartRangeFromZero = true,
                    Header = "Balance"
                }
            };

            var hashrateAxis = new NumericalAxis
            {
                ShowTrackBallInfo = true,
                StartRangeFromZero = true,
                Header = "Hashrate"
            };

            var hashrateSeries = new FastLineSeries
            {
                ItemsSource = _account.HashrateHistory,
                Interior = new SolidColorBrush(SecondaryColor),
                Stroke = new SolidColorBrush(SecondaryColor),
                StrokeThickness = 2,
                Label = _account.Name + " Hashrate",
                XBindingPath = nameof(ValueAtTime.Timestamp),
                YBindingPath = nameof(ValueAtTime.Value),
                ShowTooltip = true,
                AdornmentsInfo = new ChartAdornmentInfo
                {
                    ShowMarker = true,
                    SymbolStroke = new SolidColorBrush(SecondaryColor),
                    SymbolInterior = new SolidColorBrush(SecondaryColor),
                    SymbolHeight = 10,
                    SymbolWidth = 10,
                    Symbol = ChartSymbol.Square
                },
                YAxis = hashrateAxis
            };

            var avgHashrateSeries = new FastLineSeries
            {
                ItemsSource = _account.AvgHashrateHistory,
                Interior = new SolidColorBrush(SecondaryDarkColor),
                Stroke = new SolidColorBrush(SecondaryDarkColor),
                StrokeThickness = 2,
                Label = _account.Name + " 6hr Hashrate",
                XBindingPath = nameof(ValueAtTime.Timestamp),
                YBindingPath = nameof(ValueAtTime.Value),
                ShowTooltip = true,
                AdornmentsInfo = new ChartAdornmentInfo
                {
                    ShowMarker = true,
                    SymbolStroke = new SolidColorBrush(SecondaryDarkColor),
                    SymbolInterior = new SolidColorBrush(SecondaryDarkColor),
                    SymbolHeight = 10,
                    SymbolWidth = 10,
                    Symbol = ChartSymbol.Square
                },
                YAxis = hashrateAxis
            };

            Chart.Series.Add(balanceSeries);
            Chart.Series.Add(hashrateSeries);
            Chart.Series.Add(avgHashrateSeries);
        }

        public MiningAccount Account => _account;
    }
}
