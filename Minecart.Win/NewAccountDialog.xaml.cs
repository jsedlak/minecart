﻿using Windows.UI.Xaml.Controls;
using Minecart.Win.DataModel.Config;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Minecart.Win
{
    public sealed partial class NewAccountDialog : ContentDialog
    {
        public NewAccountDialog(PoolServiceConfig[] configs)
        {
            ServiceConfigs = configs;

            InitializeComponent();
        }

        private void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (CoinTech.SelectedItem == null)
            {
                args.Cancel = true;
            }
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
        }

        public string GetAccountName() => NameTextBox.Text;

        public string GetAccountAddress() => AddressTextBox.Text;

        public string GetPoolServiceName() => ((PoolServiceConfig) CoinTech.SelectedItem).Name;

        public PoolServiceConfig[] ServiceConfigs { get; set; }
    }
}
