﻿using System.ComponentModel;
using System.Linq.Expressions;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;
using Microsoft.CSharp.RuntimeBinder;
using Syncfusion.UI.Xaml.Spreadsheet.Converter;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Minecart.Win
{
    public sealed partial class ValueBlock : UserControl
    {
        public ValueBlock(INotifyPropertyChanged target, string headerText, params dynamic[] properties)
        {
            InitializeComponent();

            HeaderText.Text = headerText;

            foreach (dynamic prop in properties)
            {
                var tb = new TextBlock
                {
                    Foreground = Application.Current.Resources["Text"] as SolidColorBrush,
                    FontSize = 16,
                    TextAlignment = TextAlignment.Center,
                    Margin = new Thickness(0, 0, 0, 0)
                };

                var binding = new Binding
                {
                    Source = target,
                    Mode = BindingMode.OneWay,
                    Path = new PropertyPath(prop.name),
                };

                try
                {
                    if (prop.converter != null)
                    {
                        binding.Converter = prop.converter;
                        binding.ConverterParameter = prop.converterParameter;
                    }
                }
                catch (RuntimeBinderException) { }

                tb.SetBinding(TextBlock.TextProperty, binding);

                MainPanel.Children.Add(tb);
            }
        }
    }
}
