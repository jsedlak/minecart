﻿using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Minecart.Win.DataModel;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Minecart.Win
{
    public sealed partial class DashboardHub : UserControl
    {
        public DashboardHub(ObservableCollection<MiningAccount> accountsCollection)
        {
            this.InitializeComponent();

            MainGrid.ItemsSource = accountsCollection;
        }
    }
}
