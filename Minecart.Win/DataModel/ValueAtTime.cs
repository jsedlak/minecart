﻿using System;

namespace Minecart.Win.DataModel
{
    public class ValueAtTime
    {
        public ValueAtTime()
        {
            
        }

        public ValueAtTime(double value)
            : this(value, DateTime.Now)
        {
        }

        public ValueAtTime(double value, DateTime timestamp)
        {
            Value = value;
            Timestamp = timestamp;
        }

        public double Value { get; set; }

        public DateTime Timestamp { get; set; } = DateTime.Now;
    }
}