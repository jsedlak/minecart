﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Minecart.DataModel;
using Minecart.ServiceModel;
using Minecart.Win.Annotations;
using Newtonsoft.Json;

namespace Minecart.Win.DataModel
{
    public class MiningAccount : INotifyPropertyChanged
    {
        private string _name;
        private string _address;
        private double _avgHashrate;
        private double _currentHashrate;
        private double _currentBalance;
        private double _unconfirmedBalance;
        private EarningsEstimate _estimate;

        public MiningAccount()
        {
            
        }

        public MiningAccount(string name, string address)
            : this(name, address, null)
        {
            
        }

        public MiningAccount(string name, string address, string poolServiceName)
        {
            Name = name;
            Address = address;
            PoolServiceName = poolServiceName;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public string Address
        {
            get => _address;
            set
            {
                _address = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<ValueAtTime> BalanceHistory { get; } = new ObservableCollection<ValueAtTime>();

        public ObservableCollection<ValueAtTime> HashrateHistory { get; } = new ObservableCollection<ValueAtTime>();

        public ObservableCollection<ValueAtTime> AvgHashrateHistory { get; } = new ObservableCollection<ValueAtTime>();

        public string PoolServiceName { get; set; }

        public double UnconfirmedBalance
        {
            get => _unconfirmedBalance;
            set
            {
                _unconfirmedBalance = value;
                OnPropertyChanged();
            }
        }

        public double AvgHashrate
        {
            get => _avgHashrate;
            set
            {
                _avgHashrate = value;
                OnPropertyChanged();
            }
        }

        public double CurrentHashrate
        {
            get => _currentHashrate;
            set
            {
                _currentHashrate = value;
                OnPropertyChanged();
            }
        }

        public double CurrentBalance
        {
            get => _currentBalance;
            set
            {
                _currentBalance = value;
                OnPropertyChanged();
            }
        }

        public EarningsEstimate CurrentEstimate
        {
            get => _estimate;
            set
            {
                _estimate = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
    }
}
