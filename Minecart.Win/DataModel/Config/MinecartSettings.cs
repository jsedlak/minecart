﻿using System.Collections.Generic;
using Minecart.Services;
using Newtonsoft.Json;

namespace Minecart.Win.DataModel.Config
{
    public class MinecartSettings
    {
        [JsonProperty("interval")]
        public int Interval { get; set; }

        [JsonProperty("seriesType")]
        public string SeriesType { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("services")]
        public PoolServiceConfig[] Services { get; set; } = new PoolServiceConfig[]{};

        [JsonProperty("accounts")]
        public List<MiningAccountConfig> Accounts { get; set; } = new List<MiningAccountConfig>();
    }
}
