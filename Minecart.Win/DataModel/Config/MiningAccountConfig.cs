﻿using Newtonsoft.Json;

namespace Minecart.Win.DataModel.Config
{
    public class MiningAccountConfig
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("filename")]
        public string Filename { get; set; }
    }
}