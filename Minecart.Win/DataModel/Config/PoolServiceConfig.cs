﻿using Newtonsoft.Json;

namespace Minecart.Win.DataModel.Config
{
    public class PoolServiceConfig
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("coin")]
        public string Coin { get; set; }

        [JsonProperty("serviceType")]
        public string ServiceType { get; set; }
    }
}