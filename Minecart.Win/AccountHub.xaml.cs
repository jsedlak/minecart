﻿using Windows.UI.Xaml.Controls;
using Minecart.Win.DataModel;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Minecart.Win
{
    public sealed partial class AccountHub : UserControl
    {
        public AccountHub(MiningAccount account)
        {
            InitializeComponent();

            Initialize(account);
        }

        public void Initialize(MiningAccount account)
        {
            MainPanel.AddHubControls(account);
        }
    }
}
