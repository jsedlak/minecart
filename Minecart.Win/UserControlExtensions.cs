﻿using Windows.UI.Xaml.Controls;
using Minecart.Win.DataModel;

namespace Minecart.Win
{
    public static class UserControlExtensions
    {
        public static void AddHubControls(this Panel target, MiningAccount account)
        {
            void AddTileHub(string __header, dynamic[] __properties)
            {
                target.Children.Add(new ValueBlock(account, __header, __properties));
            }

            AddTileHub("Hashrate", new dynamic[]
            {
                new { name = nameof(MiningAccount.CurrentHashrate) }
            });

            AddTileHub("6hr Avg", new dynamic[]
            {
                new { name = nameof(MiningAccount.AvgHashrate) }
            });

            AddTileHub("Balance", new dynamic[]
            {
                new { name = nameof(MiningAccount.CurrentBalance) }
            });

            AddTileHub("Unconfirmed", new dynamic[]
            {
                new { name = nameof(MiningAccount.UnconfirmedBalance) }
            });

            AddTileHub("$/Day", new dynamic[]
            {
                new { name = "CurrentEstimate.Day.Coins" },
                new { name = "CurrentEstimate.Day.Dollars", converter = new StringFormatConverter(), converterParameter = "${0:#.###} USD" }
            });

            AddTileHub("$/Week", new dynamic[]
            {
                new { name = "CurrentEstimate.Week.Coins" },
                new { name = "CurrentEstimate.Week.Dollars", converter = new StringFormatConverter(), converterParameter = "${0:#.###} USD" }
            });

            AddTileHub("$/Month", new dynamic[]
            {
                new { name = "CurrentEstimate.Month.Coins" },
                new { name = "CurrentEstimate.Month.Dollars", converter = new StringFormatConverter(), converterParameter = "${0:#.###} USD" }
            });
        }
    }
}
