﻿using System;
using System.Threading.Tasks;

namespace Minecart.Win.ServiceModel
{
    public interface IConfigService
    {
        Task<TConfigModel> Load<TConfigModel>(string filename, Func<TConfigModel> createDefault);
        Task Save<TConfigModel>(string filename, TConfigModel configModel);
    }
}
